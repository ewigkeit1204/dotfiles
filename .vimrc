execute pathogen#infect()
let g:terraform_fmt_on_save=1
set number
set fileencodings=utf-8,sjis,euc-jp,latin1
set autoindent
set tabstop=2
set shiftwidth=2
set expandtab

